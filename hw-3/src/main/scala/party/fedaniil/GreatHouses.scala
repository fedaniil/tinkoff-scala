package party.fedaniil

sealed trait GreatHouse {
  def name: String
  def wealth: Wealth
  def abilityMap: Map[String, () => Wealth]
}

final case class Targaryen(
                            override val wealth: Wealth
                          ) extends GreatHouse, MakeWildFire, CallDragon {
  override val name = "Targaryen"
  override val abilityMap: Map[String, () => Wealth] = Map(
    "makeWildFire" -> makeWildFire,
    "callDragon" -> callDragon
  )
}

final case class Lannisters(
                             override val wealth: Wealth
                           ) extends GreatHouse, MakeWildFire, BorrowMoney {
  override val name = "Lannisters"
  override val abilityMap: Map[String, () => Wealth] = Map(
    "makeWildFire" -> makeWildFire,
    "borrowMoney" -> borrowMoney
  )
}
