package party.fedaniil

case object MainApp {
  def main(args: Array[String]): Unit = {
    val game = GameOfThrones(Targaryen(GreatWealth(80, 120)), Lannisters(GreatWealth(140, 80)))
    val actions = ("callDragon", "makeWildFire") :: ("makeWildFire", "borrowMoney") :: Nil
    println(actions.foldLeft(Right(game))(
      (actionResult: Either[String, GameOfThrones], action: (String, String)) =>
        actionResult.flatMap(game => game.nextTurn(action._1)(action._2) match
          case Some(value) => Right(value)
          case None => Left(action.toString() + " action is incorrect")
        )
    ))
    // Expected:
    // Right(GameOfThrones(Targaryen(GreatWealth(80.0,290)),Lannisters(GreatWealth(160.0,130)),2))
  }
}
