package party.fedaniil

trait Wealth {
  def money: Double
  def army: Int
}

final case class GreatWealth(
                              override val money: Double,
                              override val army: Int
                            ) extends Wealth
