package party.fedaniil

case class GameOfThrones(targaryen: Targaryen, lannisters: Lannisters, turn: Int = 0) {
  protected def nextTurn(targaryenStrategy: () => Wealth)(lannistersStrategy: () => Wealth): GameOfThrones = {
    GameOfThrones(Targaryen(targaryenStrategy()), Lannisters(lannistersStrategy()), turn + 1)
  }

  def nextTurn(targaryenStrategyStr: String)(lannistersStrategyStr: String): Option[GameOfThrones] = {
    for {
      targaryenStrategy <- targaryen.abilityMap.get(targaryenStrategyStr)
      lannistersStrategy <- lannisters.abilityMap.get(lannistersStrategyStr)
    } yield nextTurn(targaryenStrategy)(lannistersStrategy)
  }
}
