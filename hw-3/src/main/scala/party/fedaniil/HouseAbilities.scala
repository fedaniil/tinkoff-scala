package party.fedaniil

trait MakeWildFire {
  this: GreatHouse =>
  def makeWildFire(): Wealth = GreatWealth(wealth.money, wealth.army + 50)
}

trait BorrowMoney {
  this: GreatHouse =>
  def borrowMoney(): Wealth = GreatWealth(wealth.money + 20, wealth.army)
}

trait CallDragon {
  this: GreatHouse =>
  def callDragon(): Wealth = GreatWealth(wealth.money, wealth.army * 2)
}
